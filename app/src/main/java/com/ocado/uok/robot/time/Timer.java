package com.ocado.uok.robot.time;

import java.util.Date;

/**
 * Pomiar czasu wykonywania zadania
 */
public class Timer {

    private long startTime;

    public void start() {
        this.startTime = now();
    }

    public TimeRecord end() {
        long endTime = now();
        return new TimeRecord(startTime, endTime);
    }

    private long now() {
        return new Date().getTime();
    }
}

