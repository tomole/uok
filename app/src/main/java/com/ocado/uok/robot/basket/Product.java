package com.ocado.uok.robot.basket;


import com.ocado.uok.robot.R;

import java.util.ArrayList;
import java.util.Random;

/**
 * Produkt wraz ze zdjęciem
 */
public enum Product {

    APPLE(R.drawable.ic_apple),
    BANANA(R.drawable.ic_banana),
    ORANGE(R.drawable.ic_orange),
    ONION(R.drawable.ic_onion),
    CUCUMBER(R.drawable.ic_cucumber);

    private int id;

    Product(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Product of(String text) {
        try {
            return Product.valueOf(text.toUpperCase());
        }
        catch (IllegalArgumentException ex) {
            System.err.println("Illegal argument " + text);
            return null;
        }
    }

    private static Random random = new Random();

    /**
     * Buduje losową listę produktów do zeskanowania.
     */
    public static ArrayList<Product> buildRandomList() {
        int productsCount = values().length;

        int resultLength = random.nextInt(10) + 1;
        ArrayList<Product> result = new ArrayList<>();
        for (int i = 0; i < resultLength; i++) {
            int lot = random.nextInt(productsCount);
            result.add(values()[lot]);
        }
        return result;
    }

    /**
     * Sprawdza, czy pierwsza lista zawiera wszystkie elementy drugiej przy uwzględnieniu liczności elementów.
     */
    public static boolean firstContainsSecond(ArrayList<Product> first, ArrayList<Product> second) {
        ArrayList<Product> firstCloned = (ArrayList<Product>) first.clone();
        for (Product p: second) {
            if (!firstCloned.remove(p)) {
                return false;
            }
        }
        return true;
    }
}
