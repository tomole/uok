package com.ocado.uok.robot.result;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ocado.uok.robot.R;
import com.ocado.uok.robot.time.TimeRecord;

import java.text.SimpleDateFormat;

import static android.view.Gravity.RIGHT;

/**
 * Rysuje tabelkę wyników wraz z nagłówkiem. Tabelka jest pusta. Należy ją uzupełnić samemu.
 */
public class ResultsFragment extends Fragment {

    RelativeLayout resultsLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_results, container, false);
        resultsLayout = view.findViewById(R.id.resultsLayout);
        return resultsLayout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Tworzy wiersz tabeli na podstawie wyniku zadania, pozycji w tabeli oraz aktywności
     */
    public TableRow createTableRow(int position, TimeRecord timeRecord, Activity activity) {
        int width = getWidth(activity);

        TableRow l = createScoreRow(activity);
        l.addView(createCell(position + ".", width / 8, activity));
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        l.addView(createCell(dateFormat.format(timeRecord.getStartTimestamp()), width / 3, activity));
        l.addView(createCell(timeRecord.getReadableDuration(), width / 4, activity));
        return l;
    }

    private int getWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    private TextView createCell(String value, int width, Activity activity) {
        TextView tv = new TextView(activity);
        tv.setMinWidth(width);
        tv.setGravity(RIGHT);
        tv.setText(value);
        return tv;
    }

    private TableRow createScoreRow(Activity activity) {
        return new TableRow(activity);
    }
}
